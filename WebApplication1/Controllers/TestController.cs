﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Html()
        {
            return View();
        }
        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}